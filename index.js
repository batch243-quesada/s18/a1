// console.log('Hello World');

// 	1.  Create a function which will be able to add two numbers.
// 		-Numbers must be provided as arguments.
// 		-Display the result of the addition in our console.
// 		-function should only display result. It should not return anything.

// 		Create a function which will be able to subtract two numbers.
// 		-Numbers must be provided as arguments.
// 		-Display the result of subtraction in our console.
// 		-function should only display result. It should not return anything.

// 		-invoke and pass 2 arguments to the addition function
// 		-invoke and pass 2 arguments to the subtraction function
		
// 1. Two Sum
const getSum = (num1, num2) => {
	console.log(`Displayed sum of ${num1} and ${num2}`);
	console.log(num1 + num2);
};

getSum(5, 15);

// 1. Two Diff
const getDifference = (num1, num2) => {
	console.log(`Displayed difference of ${num1} and ${num2}`);
	console.log(num1 - num2);
};

getDifference(20, 5);

// 	2.  Create a function which will be able to multiply two numbers.
// 			-Numbers must be provided as arguments.
// 			-Return the result of the multiplication.

// 2. Two Product
const getProduct = (num1, num2) => {
	console.log(`The product of ${num1} and ${num2}:`);
	return num1 * num2;
};

console.log(getProduct(50, 10));

// 		Create a function which will be able to divide two numbers.
// 			-Numbers must be provided as arguments.
// 			-Return the result of the division.

// 2. Two Quotient
const getQoutient = (num1, num2) => {
	console.log(`The qoutient of ${num1} and ${num2}:`);
	return num1 / num2;
};

console.log(getQoutient(50, 10));

// 	 	Create a global variable called outside of the function called product.
// 			-This product variable should be able to receive and store the result of multiplication function.

// 		Create a global variable called outside of the function called quotient.
// 			-This quotient variable should be able to receive and store the result of division function.

// 		Log the value of product variable in the console.
// 		Log the value of quotient variable in the console.

const product = (num1, num2) => {
	console.log(`The product of global variables ${num1} and ${num2}:`);
	return num1 * num2;
};

let o = 50, p = 10;
console.log(product(o, p));

//

const qoutient = (num1, num2) => {
	console.log(`The qoutient of global variables ${num1} and ${num2}:`);
	return num1 / num2;
};

let a = 50, b = 10;
console.log(qoutient(a, b));


// 	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
// 			-a number should be provided as an argument.
// 			-look up the formula for calculating the area of a circle with a provided/given radius.
// 			-look up the use of the exponent operator.
// 			-you can save the value of the calculation in a variable.
// 			-return the result of the area calculation.

// 		Create a global variable called outside of the function called circleArea.
// 			-This variable should be able to receive and store the result of the circle area calculation.

// 	Log the value of the circleArea variable in the console.
const circleArea = (num1, pi) => {
	console.log(`The result of getting the area of a circle with ${num1} radius: `);
	return pi * num1 * num1;
};

let radius = 15, pi = 3.1416;
console.log(circleArea(radius, pi));

// 	4. 	Create a function which will be able to get total average of four numbers.
// 			-4 numbers should be provided as an argument.
// 			-look up the formula for calculating the average of numbers.
// 			-you can save the value of the calculation in a variable.
// 			-return the result of the average calculation.
const getAverage = (num1, num2, num3, num4) => {
	console.log(`The average of ${num1}, ${num2}, ${num3}, and ${num4}:`);
	return ave = (num1 + num2 + num3 + num4) / 4;
};

console.log(getAverage(20, 40, 60, 80));

// 	    Create a global variable called outside of the function called averageVar.
// 			-This variable should be able to receive and store the result of the average calculation
// 			-Log the value of the averageVar variable in the console.
const averageVar = (num1, num2, num3, num4) => {
	console.log(`The average of global variables ${num1}, ${num2}, ${num3}, and ${num4}:`);
	return ave = (num1 + num2 + num3 + num4) / 4;
};

let e = 20, f = 40, g = 60, h = 80;
console.log(getAverage(e, f, g, h));

// 	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 			-this function should take 2 numbers as an argument, your score and the total score.
// 			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 			-return the value of the variable isPassed.
// 			-This function should return a boolean.
const getScoreStatus = (score, total) => {
	console.log(`Is ${score}/${total} a passing score?`)
	let passingPercentage = 75;
	let percentScore = (score / total) * 100;
	return isPassed = percentScore >= passingPercentage;
}

console.log(getScoreStatus(38, 50));

// 		Create a global variable called outside of the function called isPassingScore.
// 			-This variable should be able to receive and store the boolean result of the checker function.
// 			-Log the value of the isPassingScore variable in the console.
const isPassingScore = (score) => {
	console.log(`Is ${score}/${total} a passing score?`)
	let passingPercentage = 75;
	let percentScore = (score / total) * 100;
	return isPassed = percentScore >= passingPercentage;
}

let s = 38, t = 50;
console.log(getScoreStatus(s, t));